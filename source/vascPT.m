% MRAPT.m
%
% //brief:
% MRA image processing tool
% This is a main script for processing MRA cardiac images
% methods include thresholding, morops, CC analysis, surface
% rendering, *.xml file exporting
% NOTE;  edits are only allowed at EDIT HERE!
%
% //(c) Judith Zimmermann
% date 09/18/13


function [fvVASC] = vascPT

disp('running vascPT ...'); drawnow;

global directoryMRA;

%% LOAD
[imvol, dicinfo] = dic2stack(directoryMRA);


%% PROCESS

% (1) PRE PROCESS
% normalize
imvol = mat2gray(double(imvol));
% pre smoothing
imvol_smoothed = smooth(imvol, 'median'); % 2nd param: isotropic, median, anisotropic


% (2) SEGMENTATION
threshold = findThreshold(imvol_smoothed, [0.2 0.6]);
imvol_t = doThresholding(imvol_smoothed, threshold);
close;

% (3) POST PROCESS
%get cluster with highest intensity, corresponds to blood pool in CEMRA
blv = imvol_t;
blv(imvol_t ~= max(imvol_t(:))) = 0;
blv(blv==max(blv(:))) = 1;

%fill holes
mask = [0 1 1 1 1 1 0; 0 1 1 1 1 1 0; 0 1 1 1 1 1 0]; %EDIT HERE!
mask3D = cat(3,mask,mask,mask, mask);
blv_pp3d = imclose(imopen(blv,mask3D), mask3D);

%run connected component analysis
[blv_pp3d_cca, numLabels] = bwlabeln(blv_pp3d); 
componentVec = zeros(numLabels,1);
for i = 1:numLabels
    componentVec(i) = length(find(blv_pp3d_cca(:) == i));    
end

% choose biggest component
compLabel = find(componentVec==max(componentVec));
imvol_singleComp = blv_pp3d_cca;
imvol_singleComp(imvol_singleComp~=compLabel) = 0;
imvol_singleComp = mat2gray(imvol_singleComp);



%% CREATE SURFACE
% Creating surface mesh
fvVASC = create_iso(imvol_singleComp,0.1);
fvVASC.surfaceType = 'VASC';

% Coordinate Transformation
% NOTE: 2nd param: thickness one matrix slice (out of volume) represents
fvVASC.vertices = transform2scannerMod(dicinfo, dicinfo(1).SliceThickness, fvVASC.vertices);

% render
renderSurface(fvVASC, 'red', 'non');

disp('finished vascPT ...'); drawnow;

end
