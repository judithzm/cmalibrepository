% doLinearSmoothing

% brief:
% performs isotropic (return I_heateq), anisotropic(return I_edge), and median (return I_median) smoothing on <singleImage> 
% INPUT:  singleImge
% RETURN: smoothed image

% (c) Judith Zimmermann
% date 10/22/2013

function [I_aniso] = doAnisotropicDiffusion( Image, sigma )


% Make copies for all different outcomes
I_aniso = Image;

% Define all parameters
%Define number of time steps
steps = 25;
%Define stepsize for time steps
tau = 0.1;
%Define Gaussian-kernels for edge stopping diffusion
G_edge = fspecial('gaussian',3,0.5);


% B. Iterative filtering
for i = 1:steps
    
    %%% Anisotropic diffusion filter
    % 1. Compute gradient
    [dxI_edge, dyI_edge] = grad(double(I_aniso)); 
   
    % 2. Compute diffusion coefficient (see equation (5))
    % i. Filter the image with the gaussian  filter G_edge
    I_aniso_smooth = imfilter(I_aniso,G_edge,'symmetric');
    % ii. Compute the respective gradient magnitude
    [dxI_edge_smooth, dyI_edge_smooth] = grad(double(I_aniso_smooth)); % Gradients smooth image
    Mag = sqrt(dxI_edge_smooth.^2 + dyI_edge_smooth.^2);
    % iii. Compute the diffusion coefficient
    g   = 1./(1 + (Mag./sigma).^2); % sigma = k Choice of sigma is crucial (too small - noisy, too high - blurry) 
    
    % 3. Get the modified Laplacian
    D_edge = div(g.*dxI_edge, g.*dyI_edge);
    % 3. forward Euler step for heat equation
    I_aniso = I_aniso + tau.*(D_edge);
               
end

end

