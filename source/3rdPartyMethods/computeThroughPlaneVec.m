% throughPlaneVec.m

% ** brief:
% computing throughPlaneVec for stack of dicoms
% returning normalized vector throughPlaneVec pointing towards last slice
% plotting throughPlaneVec, in plane orientation, initial slice positions,
% all based on dicom header information

% (c) J Zimmermann, 2013


function [ throughPlaneVec ] = computeThroughPlaneVec( dicinfo )

% get center points for each slice
slicePositions = zeros(3, length(dicinfo));

for i = 1: length(dicinfo)
slicePositions(:,i) = dicinfo(i).ImagePositionPatient;
end

% define first to last vector
vector = slicePositions(:,length(slicePositions)) - slicePositions(:,1);

% define throughPlaneVec, by checking dot product between slice normal and
% vector
normal = cross( dicinfo(1).ImageOrientationPatient(1:3), dicinfo(1).ImageOrientationPatient(4:6) );

% NORMALIZE!
normal = normal./norm(normal);

if (dot(normal, vector) > 0)
    throughPlaneVec = normal;
else
    throughPlaneVec = -normal;
end

scatter3(slicePositions(1, :), slicePositions(2, :), slicePositions(3, :));
hold on;
quiver3(slicePositions(1, 1), slicePositions(2, 1), slicePositions(3, 1), 100*throughPlaneVec(1), 100*throughPlaneVec(2), 100*throughPlaneVec(3), 'r' )
%add scanner reference system
quiver3(slicePositions(1, 1), slicePositions(2, 1), slicePositions(3, 1), 50,0,0,'r', 'LineWidth', 5);
quiver3(slicePositions(1, 1), slicePositions(2, 1), slicePositions(3, 1), 0,50,0,'g', 'LineWidth', 5);
quiver3(slicePositions(1, 1), slicePositions(2, 1), slicePositions(3, 1), 0,0,50,'b', 'LineWidth', 5);
% add in plane inital orientation vecs for each slice
for i= 1:length(dicinfo)
    quiver3(slicePositions(1, i), slicePositions(2, i), slicePositions(3, i), 10*dicinfo(i).ImageOrientationPatient(1), 10*dicinfo(i).ImageOrientationPatient(2), 10*dicinfo(i).ImageOrientationPatient(3), 'r');
    quiver3(slicePositions(1, i), slicePositions(2, i), slicePositions(3, i), 10*dicinfo(i).ImageOrientationPatient(4), 10*dicinfo(i).ImageOrientationPatient(5), 10*dicinfo(i).ImageOrientationPatient(6), 'g');  
end
axis equal;
title('SlicePosition (upper left image corner) scattered in 3D scanner coordinates');
drawnow;

end

