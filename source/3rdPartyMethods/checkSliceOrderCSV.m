% checkSliceOrderCSV.m for SHORT AXIS SLICES

% brief:
% checking for anatomical ROI order (whether apex to base order is true) in SHORT AXIS slices
% INPUT:  filename name of csv file
%         directory of <filename> to read
% RETURN: apex2base flag indicating order of slices, true: apex to base order,
% false: base to apex order

% (c) Judith Zimmermann
% date 10/22/2013

function [ apex2base] = checkSliceOrderCSV( filename, dataDirectory )


% define directory
fileDirectory = fullfile(dataDirectory, filename); %all csv file from patient <foldernameInput> sit in this directory

% get roiCenter Z coordinate
helperFullData = csvread(fileDirectory, 1, 0);
roiCenterZ = helperFullData(:, 10); 

% here: assumption made: increasing z value == closer to base,
zDiff = roiCenterZ(end) - roiCenterZ(1);
if zDiff > 0
    apex2base = 1;
else
    apex2base = 0;
end


end

