% readCSV.m

% NOTE: THIS IS FOR READING ONE SINGLE CSV FILE (==filename) INCLDING AS MANY ROIS AS
% THERE ARE !

% INPUT:    filename
%           dicinfo: dicominfo struct (including all dicoms for that series)  


% RETURN:   3D matrix (size of original image stack), binary mask - not all
% slices have to have a mask, some of them might of multiple non connected
% masks

% (c) Judith Zimmermann
% date 12/04/2013 


function [mask] = readCSV(filename, dicinfo, dataDirectory)


% define directory
fileDirectory = fullfile(dataDirectory, filename); %all csv file from patient <foldernameInput> sit in this directory

% get return matix resolution
rows = dicinfo(1).Rows;
cols = dicinfo(1).Columns;
numSlices = length(dicinfo);

% init return matrix
mask = zeros(rows, cols, numSlices);
[gridCols, gridRows] = meshgrid(1:cols, 1:rows); 

% read entire csv file
helperFullData = csvread(fileDirectory, 1, 0);

% get info
sliceIndices = helperFullData(:, 1) + 1; % col 1 == ImageNo
numROIpoints = helperFullData(:, 14); % col 14 == NumOfPoints per ROI

%for every ROI, read pixel points(row,col)
for i = 1:length(sliceIndices)
    
    % get points for considered ROI == read one row of csv  file
    currentROI = helperFullData( i, 15:(14+(5*numROIpoints(i))) );
    currentROI = reshape(currentROI, 5, numROIpoints(i));
    currentROI(1:3, :) = []; % discard scanner coordinates
    
    currentROImask = inpolygon(double(gridCols),double(gridRows), currentROI(1,:), currentROI(2,:));
      
    % fill in mask
    mask(:,:,sliceIndices(i)) = mask(:,:,sliceIndices(i)) + currentROImask;
    
end
 

end %% end of readCSV method

