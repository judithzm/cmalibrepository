function [ cutoffIntensity ] = calculateOutlierCutoff( I, percentage )

%number of pixel intensities/bins
L = max(I(:)) - min(I(:)) + 1; 

n = length(I(:)); % number of pixels

% get density distribution
intensityDist = hist(I(:),L);

%cumulative density function
cdf = zeros(1,L);
for i = 1:length(intensityDist)
    cdf(i) = sum(intensityDist(1:i));
end

%cut off percentage% upper outliers
cutoffIntensity = find(cdf > percentage*n);
cutoffIntensity = cutoffIntensity(1);

end

