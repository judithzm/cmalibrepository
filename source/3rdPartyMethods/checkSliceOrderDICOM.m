% checkSliceOrderDICOM.m
% 
% **brief:
% checking for apex to base order in SHORT AXIS dicoms (stack)
%
% params: dicinfo dicom headers for SA stack
% return: apex2base flag indicating order. true: apex to base order, false: base to apex order.
% 
% (c) J Zimmermann 2014
% CMAlib 


function [ apex2base ] = checkSliceOrderDICOM( dicinfo )

zDiff = dicinfo(end).ImagePositionPatient(3) - dicinfo(1).ImagePositionPatient(3);

% here: assumption made: increasing z value == closer to base,
if zDiff > 0
    apex2base = 1;
else
    apex2base = 0;
end

end

