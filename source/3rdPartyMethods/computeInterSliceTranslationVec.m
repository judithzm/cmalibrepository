% computeInterSliceTranslationVec.m

% ** brief:
% for every slice (slicePosition), the shift wrt the throughPlaneVec is computed.

% ** param:
% dicinfo all dicomheaders (one per slice)
% translationVec: set of translation Vectors (3xnumberSlices)

% (c) J Zimmermann, 01/2014

function [ translationVec ] = computeInterSliceTranslationVec( dicinfo )

% define return
translationVec = zeros(3, length(dicinfo));
x = zeros(3, length(dicinfo));

% compute throughPlaneVec (direction vec going from first to last slice)
throughPlaneVec = computeThroughPlaneVec( dicinfo ); % unit length

% fix point in scannerCoo, slicePosition first dicom
fixPoint = dicinfo(1).ImagePositionPatient;

% for each slice (2nd ... last, 1st is fix), compute translation shift
for i = 2 : length(dicinfo)
    
    % get current slicePosition
    slicePosition = dicinfo(i).ImagePositionPatient;
    
    % compute closest point on line to slicePosition
    x(:,i) = fixPoint + ( dot((slicePosition - fixPoint), throughPlaneVec)* throughPlaneVec );
    
    sliceTranslation = x(:,i) - slicePosition;
    
    translationVec(:,i) = sliceTranslation;
        
end

 

