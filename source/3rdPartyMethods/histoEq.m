function [ imvolEq ] = histoEq( imvol, minCutoff, maxCutoff )


%stupid work around...
imvolhelper = imvol;

% minIntensity = 0;
minIntensity = calculateOutlierCutoff(imvolhelper, minCutoff);
maxIntensity = calculateOutlierCutoff(imvolhelper, maxCutoff);  % neglecting intensity shift by 1 due to matlab indexing (starting at 1 rather than 0)

%number of pixel intensities/bins
L = max(imvolhelper(:)) - min(imvolhelper(:)) + 1; 

% set any pixel intensities greater than max to max intensity
imvolhelper(imvolhelper>maxIntensity) = maxIntensity;

% set any pixel intensities smaller than min to min intensity
imvolhelper(imvolhelper<minIntensity) = minIntensity;

imvolEq = (imvolhelper-minIntensity)*(L-1)/(maxIntensity-minIntensity);

%show equalizaton
imvolhisto = hist(imvol(:),L);
imvolEqhisto = hist(imvolEq(:),L);
figure;
hold on;
plot(imvolEqhisto);
plot(imvolhisto, 'r');


end