% readCSV.m

% NOTE: input data are csv files containing scanner coordinates of region
% boundaries. These boundaries are to be drawn manually  using OsiriX ROI
% plugin. Save ROIs as csv files and import them into pwd.

% INPUT: foldernameInput: patient folder name
%        sliceIndicator: vector of indices specifying different slices within same patient folder

% RETURN: masks: all masks, one binary mask per slice

% (c) Judith Zimmermann
% date 10/31/2013 


function [masks] = readCSVmultifile(foldernameInput, sliceIndicator, dicinfo)

% define directory
directory = fullfile(pwd, 'InputDataROI', foldernameInput); %all csv file from patient <foldernameInput> sit in this director

% get mask resolution
rows = dicinfo(1).Rows;
cols = dicinfo(1).Columns; 

for i=1:length(sliceIndicator)
   
        currentFilename = strcat(foldernameInput, num2str(sliceIndicator(i)), '.csv');

        % extract points from csv file (for this slice)
        fileDirectory = fullfile(directory, currentFilename); 
        points = csvread(fileDirectory, 1, 15); %start reading at row 1, col 15
        
        numberPoints = size(points,2)/5; % each point in space consists of 5 entries (3 scanner coo, 2 xy pixel position wrt current plane)
        pointCloud_singleSlice = reshape(points(1,:), 5, numberPoints);

        %reduce to pixel coordinates, discard xyz scanner coordinates
        pointCloud_singleSlice(1:3,:) = [];

        % create 2D mask of pointCloud_singleSlice pixel coordinates
        colIndicator = 1:cols;
        rowIndicator = 1:rows;
        [C,R] = meshgrid(colIndicator, rowIndicator); 

        mask_singleSlice = inpolygon(double(C),double(R), pointCloud_singleSlice(1,:), pointCloud_singleSlice(2,:));
        %update stack of masks
        masks(:,:,i) = mask_singleSlice;
end



end %% end of readCSV method

