% writeEnsiteFile.m

% **brief:
% this method takes surfaces, <patch>,  (face vertex representation) with corresponding names <patch_names> and opens and
% writes an xml file <fname>
% arbitrary number of surfaces can be written into the file, surfaces are
% colored depending surface name.
% possible names and corresponding colors:
% VASC red
% EPI blue
% LVEPI blue
% RVENDO green
% LVENDO green
% FAT yellow
% SCAR magenta
% .. if name different from those above: cyan

% params: fname filename output .xml file
%         patch cell array including all surfaces to be written in file
%         patch_names corresponding names
%         normals

% edited by J Zimmermann, 2014, CMAlib


function writeEnsiteFile(fname,patch,patch_names,normals)

FID=fopen(fname,'w');

% Write header
fprintf(FID,'%s\n','<?xml version="1.0"?>');
fprintf(FID,'%s\n','<DIF>');
fprintf(FID,'%s\n','<DIFHeader>');
fprintf(FID,'%s\n','<Version>SJM_DIF_3.0</Version>');
fprintf(FID,'%s\n','<VendorVersion>NA</VendorVersion>');
fprintf(FID,'%s\n','<PatientName>NA</PatientName>');
fprintf(FID,'%s\n','<PatientID>NA</PatientID>');
fprintf(FID,'%s\n','<PatientBirthDate>0001-01-01</PatientBirthDate>');
fprintf(FID,'%s\n','<PatientGender>NA</PatientGender>');
fprintf(FID,'%s\n','<StudyID>NA</StudyID>');
fprintf(FID,'%s\n','<SeriesNumber>NA</SeriesNumber>');
fprintf(FID,'%s\n','<StudyDate>0001-01-01</StudyDate>');
fprintf(FID,'%s\n','<StudyTime>00:00:00</StudyTime>');
fprintf(FID,'%s\n','<SeriesTime>00:00:00</SeriesTime>');
fprintf(FID,'%s\n','<Modality>NA</Modality>');
fprintf(FID,'%s\n','<RefPhysName>NA</RefPhysName>');
fprintf(FID,'%s\n','<StudyDesc>NA</StudyDesc>');
fprintf(FID,'%s\n','<SeriesDesc>NA</SeriesDesc>');
fprintf(FID,'%s\n','<OperatorName>NA</OperatorName>');
fprintf(FID,'%s\n','<OperatorComments>NA</OperatorComments>');
fprintf(FID,'%s\n','<SegmentationDate>0001-01-01</SegmentationDate>');
fprintf(FID,'%s\n','<Inventor>0</Inventor>');
fprintf(FID,'%s\n','</DIFHeader>');

% Write Body
fprintf(FID,'%s\n','<DIFBody>');

% Write volumes
fprintf(FID,'<Volumes number="%d">\n',length(patch));

% Create each patch
for i=1:length(patch)
    
    %define patch color
    switch patch_names{i}
        case 'EPI' 
            patchColor = '0000FF70';
        case 'LVEPI' 
            patchColor = '0000FF70';
        case 'LVENDO'
            patchColor = '00FF0070';          
        case 'RVENDO'
            patchColor = '00FF00FF';
            
        case 'FAT'
            patchColor = 'FFFF00FF';
        case 'SCAR'
            patchColor = 'FF00FFFF';  
            
        case 'VASC'
            patchColor = 'FF0000FF';  
        otherwise
            patchColor = '00FFFF00';
    end
        

    fprintf(FID,'<Volume name="%s" color="%s">\n',patch_names{i},patchColor);
    
    % Vertices
    fprintf(FID,'<Vertices number="%d">\n',size(patch{i}.vertices,1));

    for k=1:size(patch{i}.vertices,1)
        fprintf(FID,'%f %f %f\n',patch{i}.vertices(k,1),patch{i}.vertices(k,2),patch{i}.vertices(k,3));
    end

    fprintf(FID,'%s\n','</Vertices>');

    % Normals
    if nargin > 3

        fprintf(FID,'<Normals number="%d">\n',size(normals{i},1));

        for k=1:size(normals{i},1)
            fprintf(FID,'%f %f %f\n',normals{i}(k,1),normals{i}(k,2),normals{i}(k,3));
        end

        fprintf(FID,'%s\n','</Normals>');

    end

    % Faces
    fprintf(FID,'<Polygons number="%d">\n',size(patch{i}.faces,1));

    for k=1:size(patch{i}.faces,1)
        fprintf(FID,'%d %d %d\n',patch{i}.faces(k,1),patch{i}.faces(k,2),patch{i}.faces(k,3));
    end
    
    fprintf(FID,'%s\n','</Polygons>');

    fprintf(FID,'%s\n','</Volume>');

end

fprintf(FID,'%s\n','</Volumes>');

% Write Labels
fprintf(FID,'%s\n','<Labels number="0">');
fprintf(FID,'%s\n','</Labels>');

% ObjectMap tag is optional
% <ObjectMap>
% <Rotation> 1.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 1.000000 </Rotation>
% <Translation> 0.000000 0.000000 0.000000 </Translation>
% <Scaling> 1.000000 1.000000 1.000000 </Scaling>
% <MD5Signature>0</MD5Signature>
% </ObjectMap>

fprintf(FID,'%s\n','</DIFBody>');
fprintf(FID,'%s\n','</DIF>');

fclose(FID);

return