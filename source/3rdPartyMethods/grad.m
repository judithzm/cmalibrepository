function [dxu, dyu] = grad(u)
% [dxu dyu] = grad(u) computes the gradient of the function u
% with forward differences assuming
% Neumann boundary conditions
%
% written by
% Juan S. Osorio
% May 1st 2013
% Chair for Computer Aided Medical Procedures & Augmented Reality
% Technische Universit?t M?nchen

[m, n] = size(u);

dxu = zeros(m,n);
dyu = zeros(m,n);

%approximate the derivative of u in x-direction with forward differences
%dxu(:,1:(n-1)) = double(u(:,1:end-1))-double(u(:,2:end)) ;

dxu = [double(u(:,2:end))-double(u(:,1:end-1)) zeros(m,1)] ;

%approximate the derivative of u in y-direction with forward differences
%dyu(1:(m-1),:) = double(u(1:end-1,:))- double(u(2:end,:)) ;

dyu = [double(u(2:end,:))-double(u(1:end-1,:)); zeros(1,n)] ;
