% regionGrowing2D.m
% 
% ** brief:
% grows a region from a given seed point in 2D
% 
% params:   Image: 2D image matrix
%           seed: single seed point/ init point
%           T: threshold for computing homogeneity criteria
%           updateMean: flag indicating whether mean of region should be updated after adding new pixels
%
% return: Region 2D binary mask
% 
% (c) J Zimmermann 2014
% CMAlib


function [ Region ] = regionGrowing2D( Image, seed, T, updateMean )

%define
[s1, s2] = size(Image); 
Visited = zeros(s1,s2);
Region = zeros(s1,s2);
neighbor = [-1 0; 1 0; 0 -1;0 1; 1 1; -1 -1; -1 1; 1 -1];
t = tic;

%init
x = seed(1); % row index
y = seed(2); % col index

Imean = Image(x,y); % initial mean intensity, to be updated.
list(1,:) = [x, y]; %add seed pixel to the list


%% ****ITER: Grow until FIFO list only contains zero pixels*****************
while (size(list,1) ~= 0)       %CHANGE
    
    % Pick up first pixel from the pixel indices list
    %pixel_curr = Image(list(1,1), list(1,2));
    
    pixel_curr_cooX = list(1,1);
    pixel_curr_cooY = list(1,2);
    
    list(1,:) = []; %delete first list entry
    %lengthList = lengthList - 1;

    % Check if pixel is homogeneous, use: abs(pixel_curr - Im) <= T.
    %if(abs(pixel_curr - Imean) <= T)    
    if(abs(Image(pixel_curr_cooX, pixel_curr_cooY) - Imean) <= T) %CHANGE
        
        % label it as homogeneous, i.e. it belongs to Region
        Region(pixel_curr_cooX, pixel_curr_cooY) = 1;
        
        % add neighbors (i.e. their pixel indices) to the list
        for k = 1:8
            % check if neighbor has been visited before: if false --> add
            if( Visited( pixel_curr_cooX + neighbor(k,1) , pixel_curr_cooY + neighbor(k,2)  ) == 0 )
       
                Visited(pixel_curr_cooX + neighbor(k,1) , pixel_curr_cooY + neighbor(k,2)) = 1; 
                list = [list; pixel_curr_cooX + neighbor(k,1) , pixel_curr_cooY + neighbor(k,2)] ; %current pixel plus neighbor offset

            end
        end
    
        
        if updateMean == 1    
        % update mean intensity Im of Region
        numPixelsInRegion = nnz(Region); %including new pixel
        Imean = ( (numPixelsInRegion-1) * Imean + Image(pixel_curr_cooX , pixel_curr_cooY ) ) / numPixelsInRegion;
        end
  
    end %END homogeneity check
        
    % in case pixel is inhomogeneous: only update 'Visited' matrix
    Visited(pixel_curr_cooX, pixel_curr_cooY) = 1;    
    
    
    % DRAWING CURRENT STATUS after each iteration
%     if toc(t)>1/23
%         imagesc(Image+Region,[0 1]); axis image; colormap gray; axis off; axis xy;
%         drawnow;
%         t = tic;
%     end
    
end % end of WHILE, ITERATING FIFO


%% Final display
% imagesc(Image+Region,[0 1]); axis image; colormap gray; axis off; axis xy;
% drawnow;

end

