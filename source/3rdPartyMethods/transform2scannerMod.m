%% This function takes an Nx3 matrix of vertex points in pixel space and 
% converts them to scanner space (translation, rotation, scaling)
%
% INPUTS:
%           dicinfo: return of dic2stack, array of ALL dicom headers, one
%           per image slice
%           vertices - Nx3 matrix of vertex points
%
% OUTPUT:
%           V_trans  - Nx3 matrix of transformed vertex points

% original code: Sarah Ouadah .. transform2scanner(..)

% Interface has been modified (JZ)
% correction in terms of transformation definition has been made. (JZ)
% Implementation has been modified for better performance. (JZ)

% TODO: correct slice gap mistake ! (will give problems when dealing with gap datasets)

function [V_trans] = transform2scannerMod(dicinfo, p_sliceThickness, vertices)
    
V=vertices;

% Do scaling
V(:,1)=vertices(:,1).*dicinfo(1).PixelSpacing(1);
V(:,2)=vertices(:,2).*dicinfo(1).PixelSpacing(2);
V(:,3)=vertices(:,3).*p_sliceThickness; % THIS IS NOT CORRECT !!!! not always true... this assuemes distance factor == 0 !!!

% Define 4x4 transformation matrix

% define 3rd axis of image coo system
N = computeThroughPlaneVec(dicinfo);
N = N./norm(N);

R=[dicinfo(1).ImageOrientationPatient(1:3) dicinfo(1).ImageOrientationPatient(4:6) N];
T=eye(4);
T(1:3,1:3)=R;
T(1:3,4)=dicinfo(1).ImagePositionPatient;

% change to homog coordinates, take transpose
V(:,4)=1;
V=V.';

% do transform
V_trans = T * V;

% Recover Nx3 format, Revert back from homogeneous coordinates
V_trans = V_trans.';
V_trans = V_trans(:,1:3);

end