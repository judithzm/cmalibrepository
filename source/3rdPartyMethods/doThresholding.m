% doThresholding.m
%
% thresholds data (imvol parameter) using given threshold values, returns thresholded data imvol_t 
% 
% J Zimmermann, CMAlib

function [ imvol_t ] = doThresholding( imvol, threshold )


disp('running thresholding with threshold value:');
disp(threshold);

T = [0 threshold Inf];  

%define output of thresholding
imvol_t = zeros(size(imvol));

for k = 1:length(T)-1
    %seperate image into (length(T)-1) regions
    imvol_t(((imvol>T(k)).*(imvol<=T(k+1)))==1)=k;
end
 

end

