% doVoxelProjection.m

% ** brief:
% performs the mask projection (eg Black Blood based Myo
% mask) onto an LGE data set (of the same patient, needless to say..). this
% is needed to perform a myo-prior based sar segmentation (see doScarSegmentation.m)

% params:
% imvolSource    e.g. Black Blood
% dicinfoSource   
% imvolDest      e.g. LGE 
% dicinfoDest

% return:
% projectedValues: size of imvolDest, imvolSource Values in dest coordinate
% system

% JZimmermann, CMAlib 2014

function [ projectedValues ] = doVoxelProjection( imvolSource, dicinfoSource, imvolDest, dicinfoDest )

% define local reference systems
sourceCooSystem = buildLocalCooSystem(dicinfoSource); 
destCooSystem = buildLocalCooSystem(dicinfoDest);

% % just to show the difference
% [S,I]=iimage_3Dicom_modified(imvolSource(:,:,15), dicinfoSource(18));
% hold on
% [S,I]=iimage_3Dicom_modified(imvolDest(:,:,1), dicinfoDest(1));
% [S,I]=iimage_3Dicom_modified(imvolDest(:,:,2), dicinfoDest(2));
% [S,I]=iimage_3Dicom_modified(imvolDest(:,:,3), dicinfoDest(3));
% [S,I]=iimage_3Dicom_modified(imvolDest(:,:,4), dicinfoDest(4));
% [S,I]=iimage_3Dicom_modified(imvolDest(:,:,5), dicinfoDest(5));

%% define rotation source to dest
R = defineRotation(sourceCooSystem, destCooSystem); % FORWARD warping
R_inverse = R'; % backward warping


%% define translation source to dest
T = dicinfoSource(1).ImagePositionPatient - dicinfoDest(1).ImagePositionPatient;
T_inverse = -T; % backward warping


%% define dest based GRID (imvolDest dimnesions) ---- LGE MRI
xCooDest = 0:1:(size(imvolDest,2)-1);% cols
xCooDest = xCooDest.*dicinfoDest(1).PixelSpacing(1); 
yCooDest = 0:1:(size(imvolDest,1)-1); % rows
yCooDest = yCooDest.*dicinfoDest(1).PixelSpacing(2);
zCooDest = computeInterSliceGap( dicinfoDest );
dimDest_i = size(imvolDest,1);
dimDest_j = size(imvolDest,2);
dimDest_k = size(imvolDest,3);

% TEMP JZ
% % define some dummies (low res) to transform (because computer is to slow to render all points)
% xCooDest = 0:5:25;
% yCooDest = 0:2.5:25;
% zCooDest = [0 4 10 20];
% dimDest_i = length(xCooDest);
% dimDest_j = length(yCooDest);
% dimDest_k = length(zCooDest);
% TEMP JZ

[xGRIDdest, yGRIDdest, zGRIDdest] = meshgrid( xCooDest, yCooDest, zCooDest) ;

%% do backward TRANSFORM
% define coordinates to be transformed
destCoo = [xGRIDdest(:) yGRIDdest(:) zGRIDdest(:)]';
destCoo_translated = destCoo + repmat(T_inverse,1,length(xGRIDdest(:)));
destCoo_transformed = R_inverse * destCoo_translated; % grid points (of dest image) in source image coordinate system

%reshape into 3D matrix
xGRIDdest_transformed = reshape(destCoo_transformed(1,:), dimDest_i, dimDest_j, dimDest_k); 
yGRIDdest_transformed = reshape(destCoo_transformed(2,:), dimDest_i, dimDest_j, dimDest_k);
zGRIDdest_transformed = reshape(destCoo_transformed(3,:), dimDest_i, dimDest_j, dimDest_k);


%% TEMP plot first slice of imvol dest (pixelpoint) before and after transform - this is in the local destCooSystem!!
figure;
gridsliceDestX =  xGRIDdest(:,:,1);
gridsliceDestX = gridsliceDestX(:);
gridsliceDestY =  yGRIDdest(:,:,1);
gridsliceDestY = gridsliceDestY(:);
gridsliceDestZ =  zGRIDdest(:,:,1);
gridsliceDestZ = gridsliceDestZ(:);
scatter3(gridsliceDestX, gridsliceDestY, gridsliceDestZ)

hold on
gridsliceDestX_t =  xGRIDdest_transformed(:,:,1);
gridsliceDestX_t = gridsliceDestX_t(:);
gridsliceDestY_t =  yGRIDdest_transformed(:,:,1);
gridsliceDestY_t = gridsliceDestY_t(:);
gridsliceDestZ_t =  zGRIDdest_transformed(:,:,1);
gridsliceDestZ_t = gridsliceDestZ_t(:);
scatter3(gridsliceDestX_t, gridsliceDestY_t, gridsliceDestZ_t, 'r')
axis equal;
% end TEMP


%% GRID INTERPOLATION 

% define source based coordinates
xCooSource = 0:1:(size(imvolSource,2)-1);% cols
xCooSource = xCooSource.*dicinfoSource(1).PixelSpacing(1); 
yCooSource = 0:1:(size(imvolSource,1)-1); % rows
yCooSource = yCooSource.*dicinfoSource(1).PixelSpacing(2);
zCooSource = computeInterSliceGap( dicinfoSource );

[xGRIDsource, yGRIDsource, zGRIDsource] = meshgrid( xCooSource, yCooSource, zCooSource) ;
sourceCoo = [xGRIDsource(:) yGRIDsource(:) zGRIDsource(:)]';
% get corresponding values
sourceValues = imvolSource(:)';

%run interpolation at GRIDdest_transformed given sourceValues at specified
%sourceCoo
projectedValues = griddata3(sourceCoo(1,:),sourceCoo(2,:),sourceCoo(3,:), sourceValues, xGRIDdest_transformed,yGRIDdest_transformed,zGRIDdest_transformed,'neighbor');

end

