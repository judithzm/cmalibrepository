
% plotHistgram.m
% plotting histogran wrapper, leaving out zeroes (background pixels) for better visualization


function plotHistogram( imvol )

imvol_fg = imvol;
imvol_fg (imvol == 0) = [];
figure(3);
hist(imvol_fg(:), 200); title(strcat('Histogram forground'));

return;

end

