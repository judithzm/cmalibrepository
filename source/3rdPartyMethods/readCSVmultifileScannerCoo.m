% readCSV.m

% NOTE: input data are csv files containing scanner coordinates of region
% boundaries. These boundaries are to be drawn manually  using OsiriX ROI
% plugin. Save ROIs as csv files and import them into pwd.

% INPUT: foldernameInput: patient folder name
%        sliceIndicator: vector of indices specifying different slices within same patient folder

% RETURN: point Cloud, 3xN matrix including X, Y, Z scanner coordinates in respective rows, N
% being the number of points

% (c) Judith Zimmermann
% date 10/31/2013 


function [ pointCloud ] = readCSVmultifileScannerCoo(foldernameInput, sliceIndicator)


% define directory
directory = fullfile(pwd, 'InputDataROI', foldernameInput); %all csv file from patient <foldernameInput> sit in this director

pointCloud = [0;0;0];

for i=1:length(sliceIndicator)
   
        currentFilename = strcat(foldernameInput, num2str(sliceIndicator(i)), '.csv');

        % extract points from csv file (for this slice)
        fileDirectory = fullfile(directory, currentFilename); 
        points = csvread(fileDirectory, 1, 15); %start reading at row 1, col 15
        numberPoints = size(points,2)/5; % each point in space consists of 5 entries (3 scanner coo, 2 xy pixel position wrt current plane)
        pointCloud_singleSlice = reshape(points(1,:), 5, numberPoints);

        %reduce to pixel coordinates, discard xyz scanner coordinates
        pointCloud_singleSlice(4:5,:) = [];
        
        %update pointCloud
        pointCloud = [pointCloud pointCloud_singleSlice];

end

pointCloud(:,1) = []; %stupid workaround, (in order to be able to dynamically allocate) MISSING C++ !!


end %% end of readCSVscannerCoo method

