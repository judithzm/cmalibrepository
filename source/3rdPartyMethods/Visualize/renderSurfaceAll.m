 % renderSurfaceAll.m
% This is a wrapper for Matlab's patch(..) visualization method. Renders a
% list of fv patched with defined colors (depending on set surfaceType)
%
% 
% params: fvList face/vertices struct array
%         
% (c) Judith Zimmermann, CMAlib 2014

function renderSurfaceAll( fvList )

numberOfSurfaces = size(fvList, 2);

figure; hold on;

for surfaceIndex = 1:numberOfSurfaces
    % set colors
    switch fvList(surfaceIndex).surfaceType
        case 'EPI'
            colorFace = 'blue';
            colorEdge = 'non';
        case 'ENDO'
            colorFace = 'non';
            colorEdge = 'red';           
        case 'VASC'
            colorFace = 'red';
            colorEdge = 'non';            
        case 'FAT'
            colorFace = 'yellow';
            colorEdge = 'non';                        
        case 'SCAR'
            colorFace = 'magenta';
            colorEdge = 'non';
    end
    
    % render surface
    renderSurface(fvList(surfaceIndex), colorFace, colorEdge);

end

title('Rendering all created surfaces for this study')
camlight('right'); lighting phong; axis equal;
rot3D; plotScannerCooSystem;

end % end of renderSurfaceAll(..)

