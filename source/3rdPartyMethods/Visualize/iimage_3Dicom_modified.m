 function [S,I]=iimage_3Dicom_modified(image, dicinfo)
% This function renders a 2D DICOM image in scanner coordinates.
%
% SYNTAX : [S,I]=iimage_3Dicom(image, dicinfo)
%
% INPUTS : image (image), corresponding dicom header (dicinfo)
%
% OUTPUTS: S - Handle to the surface object
%          I - Image data
%
% original: DBE 2008.03.07

% ****MODIFIED VERSION (J Zimmermann 01/06/2014) params have changed (now image and
% corresponding dicom header)***

I = image;
H = dicinfo;
 
% Define the 2D space
DIM=double([H.Width H.Height]);
[x,y]=ndgrid(1:DIM(1),1:DIM(2));
 
% Scale the 2D
x=x.*H.PixelSpacing(1);
y=y.*H.PixelSpacing(2);
 
% Define the transformation
N=cross(H.ImageOrientationPatient(1:3),H.ImageOrientationPatient(4:6));
R=[H.ImageOrientationPatient(1:3) H.ImageOrientationPatient(4:6) N];
T=eye(4);
T(1:3,1:3)=R;
T(1:3,4)=H.ImagePositionPatient;
 
% Define the 3D space
data=[x(:) y(:) ones(size(x(:)))]';
XYZ=fe_affine_transform(data,T);
 
X=reshape(XYZ(1,:),[size(x,1) size(x,2)]);
Y=reshape(XYZ(2,:),[size(x,1) size(x,2)]);
Z=reshape(XYZ(3,:),[size(x,1) size(x,2)]);
 
% Render the slice
% fig;
S=surf(X,Y,Z,imadjust(mat2gray(I')));
% S=surf(X,Y,Z,I');
  set(S,'EdgeColor','None'); %,'FaceAlpha','Flat','AlphaDataMapping','scaled','AlphaData',(mat2gray(I')).^0.9999);
axis image xy equal tight; rot3D;
colormap(gray);
 
BG_color=[0.1 0.1 0.15];
set(gcf,'color',BG_color);
 
set(gca,'Xcolor',[0.5 0.5 0.5]);
set(gca,'Ycolor',[0.5 0.5 0.5]);
set(gca,'Zcolor',[0.5 0.5 0.5]);
set(gca,'color',BG_color);
 
return



