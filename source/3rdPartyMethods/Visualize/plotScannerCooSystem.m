function plotScannerCooSystem

%add scanner reference system
hold on;
quiver3(0,0,0, 100,0,0,'r', 'LineWidth', 5);
quiver3(0,0,0, 0,100,0,'g', 'LineWidth', 5);
quiver3(0,0,0, 0,0,100,'b', 'LineWidth', 5);
end

