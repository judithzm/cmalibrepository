% This function applies an affine transformation (4x4) to X,Y,Z data.  Data
% input can be 3xN or Nx3.
%
% SYNTAX : data=fe_affine_transform(data,affine)
%
% INPUTS : data   - 3xN or Nx3 array of Cartesian data
%          affine - 4x4 affine transformation matrix
%
% OUTPUTS: data   - Transformed data with array dimensions matching the input
%
% DBE 06/28/02

function data_trans=fe_affine_transform(data,affine)

if ~isequal(size(affine),[4 4])
  error('AFFINE transformations must be 4x4 arrays.');
end

if size(data,1)==3  %<size(data,2)
  data=data';
  transpose_data=1;
else
  transpose_data=0;
end

data_trans=(affine*[data,ones(size(data,1),1)]')';
data_trans=data_trans(:,1:3);

if transpose_data  % Should force output dimensions to be equal to input dimensions...
  data_trans=data_trans';
end

return