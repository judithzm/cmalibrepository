% This function renders an image at a spatial location as a patched
% surface.
%
% SYNTAX: image_surf(X,Y,Z,I)
%         image_surf(I);
%
% DBE 10/22/03

function p=iimage_surf(X,Y,Z,I)

set(gcf,'Renderer','OpenGL');

if nargin==1
  I=X;
  [X,Y]=meshgrid(1:size(X,2),1:size(X,1));
  Z=zeros(size(X));
end

I=squeeze(I);

% if ndims(I)==2
%   patch_color=repmat((mat2gray(double(I))),[1 1 3]);
% else
  patch_color=I;
% end

p=patch(surf2patch(X,Y,Z,patch_color));
% shading faceted;
set(p,'EdgeColor','None','FaceColor','Flat');
set(p,'FaceLighting','None');  % This prevents light object from acting on this patch surface which tends to significanty decrease contrast
axis image tight ij;
return
  