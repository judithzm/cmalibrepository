% renderSurface.m
% This is a wrapper for Matlab's patch(..) visualization method.
% 
% params: face/vertices struct
%         color: preferred color of surface mesh
%         
%         fv: returns first input param
%         
% (c) Judith Zimmermann
% 2013

function renderSurface( fv, colorFace, colorEdge)

% get rid of label
fv = rmfield(fv, 'surfaceType');

% Render surface
h = patch(fv); title('3D Surface mesh of polygon mesh');
set(h, 'FaceColor', colorFace, 'EdgeColor', colorEdge); 
camlight('right'); lighting phong; axis equal;
rot3D;

end

