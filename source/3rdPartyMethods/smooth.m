% smooth.m
% interface for multiple smoothing operations

% params
% imvol data to be smoothed, stack or single image
% type linear, median, anisotropic smoothing


function [ imvol_smoothed ] = smooth( imvol, type )

switch type
    

% SMOOTH ISOTROPIC
    case 'isotropic'
    mask = fspecial('gauss', 6, 1.5);
    imvol_iso = imvol;
    for i=1:size(imvol,3)
        imvol_iso(:,:,i) = imfilter(imvol(:,:,i), mask, 'symmetric');
    end
    imvol_smoothed = imvol_iso;

    
% SMOOTH MEDIAN
    case 'median'
    imvol_median = imvol;
    steps = 3;

    for j=1:steps
        for i = 1:size(imvol,3)
        imvol_median(:,:,i) = medfilt2(imvol_median(:,:,i), [3 3]); 
        end
    end
    imvol_smoothed = imvol_median;

  
% SMOOTH ANISOTROPIC (using gradient edge indicator)
    case 'anisotropic'
    %Define sigma for edge indicator function (anisotropic diffusion)
    sigma = 0.02; %EDIT HERE! -- around 0.01 for cardiac cines
    imvol_aniso =  zeros(size(imvol));

    for i = 1:size(imvol,3)
    %do smoothing, use: function [ I_heateq, I_edge, I_median, sigma ] = doNonLinearSmoothing( Image )
    [ imvol_aniso(:,:,i) ] = doAnisotropicDiffusion(imvol(:,:,i), sigma);
    end
    imvol_smoothed = imvol_aniso;


    otherwise  
        return;

end

