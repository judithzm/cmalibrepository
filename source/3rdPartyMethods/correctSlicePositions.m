% correctSlicePosition.m
% correct for any inter slice translation/rotation (for 2D scan: slice
% position can be modified manually bz techs)... does not account for inter
% slice SA view changes

% params: dicinfo: array of structs, each representing one dicom header

% TO BE CALLED PRIOR tO PROCESSING !!!

% retun: imvol transformed (correct imvol)
% (c) Judith Zimmermann
% 2013


function [ imvolTransformed ] = correctSlicePositions( imvol, dicinfo )

%% (1) get sliceTranslationVec in WCS
translationVec = computeInterSliceTranslationVec(dicinfo);

% transform translationVec from WCS to image coo system (ICS)

% define WCS base
wcs = [1 0 0; 0 1 0; 0 0 1]; wcs = wcs';

% define ICS base
ics = [dicinfo(1).ImageOrientationPatient(1:3)';
        dicinfo(1).ImageOrientationPatient(4:6)';
        computeThroughPlaneVec(dicinfo)'];
ics = ics';

% define rotation matrix from WCS ICS, all base vectors are unit vectors !
R_wcs2ics = defineRotation(wcs,ics);

% do WCS to ICS transform
translationVec_ics = zeros(size(translationVec));
for i = 1:size(translationVec,2)
    translationVec_ics(:,i) = R_wcs2ics * translationVec(:,i); 
end
translationVec_ics = translationVec_ics(1:2,:,:); %due to ICS, 3rd axis == throughPlaneVec


%% (2) compute rotation angles for each slice
refX_ics = [1;0]; 
angle = zeros(1,length(dicinfo));

for i = 2:size(dicinfo,3)
    % transform plane orientation vec (X) into ICS
    X_ics = R_wcs2ics * dicinfo(i).ImageOrientationPatient(1:3);
    X_ics = X_ics(1:2); % third component != 0, points towards throughPlaneVec
    
    % compute rotation angle
    angle(i) = acos(dot(refX_ics, X_ics));         
end


%% (4) do transform
imvolTransformed = imvol;

for i = 1:length(dicinfo)
% correct for translation, in ICS
sliceTranslated = image_translate(imvol(:,:,i), -translationVec_ics(:,i));

% correct for rotation, in ICS
imvolTransformed(:,:,i) = image_rotate( sliceTranslated, angle(i), [-size(sliceTranslated,2)/2 -size(sliceTranslated,1)/2] );
end


end

