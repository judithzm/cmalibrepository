% computeInterSliceGap.m
% create distance vector to determine inter-slice gaps

% params: dicinfo: array of structs, each representing one dicom header

% retun: column vector containing distance to first slice for each
% subsequent slice, i.e. distance(1) == 0; (must be !)

% (c) Judith Zimmermann
% 2013

function [ distance ] = computeInterSliceGap( dicinfo )

% get center points for each slice
centerPoints = zeros(3, length(dicinfo));
for i = 1: length(dicinfo)
centerPoints(:,i) = dicinfo(i).ImagePositionPatient;
end

% compute spatial distance wrt first slice (first slice at spatial location 0)
distance = zeros(1, length(dicinfo));
for i = 1: length(dicinfo)
distance(i) = sqrt((centerPoints(1,i) - centerPoints(1,1)).^2 + (centerPoints(2,i) - centerPoints(2,1)).^2 + (centerPoints(3,i) - centerPoints(3,1)).^2 );
end

%finalize
distance = round(distance);

end

