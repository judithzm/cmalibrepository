% doLinearSmoothing

% brief:
% performs isotropic (return I_heateq), anisotropic(return I_edge), and median (return I_median) smoothing on <singleImage> 
% INPUT:  singleImge
% RETURN: smoothed image

% (c) Judith Zimmermann
% date 10/22/2013

function [ I_heateq, I_edge, I_median ] = doNonLinearSmoothing( Image, sigma )


% Make copies for all different outcomes
I_heateq = Image;
I_edge = Image;
I_median = Image;

% Define all parameters
%Define number of time steps
steps = 50;
%Define stepsize for time steps
tau = 0.1;
%Define Gaussian-kernels for edge stopping diffusion
G_edge = fspecial('gaussian',3,0.5);


% B. Iterative filtering

for i = 1:steps
    
    %%% Isotropic diffusion filter
    % 1. compute right hand side of heat equation using grad and div
    [dxI_edge, dyI_edge] = grad(double(I_heateq));  %CHANGE grad (it was gradient)
    D_heateq = div(dxI_edge, dyI_edge);
    % 2. forward Euler step for heat equation (see equation (3))
    I_heateq = I_heateq + tau*D_heateq;
    
    
    
    %%% Anisotropic diffusion filter
    % 1. Compute gradient
    [dxI_edge, dyI_edge] = grad(double(I_edge)); 
   
    % 2. Compute diffusion coefficient (see equation (5))
    % i. Filter the image with the gaussian  filter G_edge
    I_edge_smooth = imfilter(I_edge,G_edge,'symmetric');
    % ii. Compute the respective gradient magnitude
    [dxI_edge_smooth, dyI_edge_smooth] = grad(double(I_edge_smooth)); % Gradients smooth image
    Mag = sqrt(dxI_edge_smooth.^2 + dyI_edge_smooth.^2);
    % iii. Compute the diffusion coefficient
    g   = 1./(1 + (Mag./sigma).^2); % sigma = k Choice of sigma is crucial (too small - noisy, too high - blurry) 
    
    % 3. Get the modified Laplacian
    D_edge = div(g.*dxI_edge, g.*dyI_edge);
    % 3. forward Euler step for heat equation
    I_edge = I_edge + tau.*(D_edge);
    
    
    
    %%% Iterative median filter
    % 2. Filter the image I_median with a median filter of size 5x5 (imfilt2)
    I_median = medfilt2(I_median, [5 5]);
        
    %%% Display every tenth result !
    if mod(i,10)==1
        figure(1);
        disp(i)
        subplot(2,2,1); imagesc(Image); axis image; axis off; colormap gray;...
            title('Original Image')
        
        subplot(2,2,2); imagesc(I_heateq); axis image; axis off; colormap gray;...
            title('Isotropic Diffusion') %i.e. gaussian filtering
        
        subplot(2,2,3); imagesc(I_edge); axis image; axis off; colormap gray;...
            title('Anisotropic Diffusion') %gaussian filtering including conductance parameter
        
        subplot(2,2,4); imagesc(I_median); axis image; axis off; colormap gray; ...
            title('Iterative Median Filtering') %non-linear median filtering

        drawnow
    end
    
    pause(0.1);
    
end

end

