% interpolateMask3D.m

% brief:
% perform 3D Mask interpolation, accounts for gaps between subsequent slices

% params:
% M binary mask to be interpolated, stack of segmented slices
% dicinfo array of struct, length(dicinfo) == size(M,3)

% return:
% BLVinterp3 interpolated 3D volume, ready for surface rendering

% (c) Judith Zimmermann
% date 12/06/2013


function [ BLVinterp3, sliceThicknessVirtual ] = interpolateMask3D( M, dicinfo )
%% MASK INTERPOLATION

% get distance vector, locate z position of extracted slices
% z-direction == perpendicular to frame plane
distance = computeInterSliceGap( dicinfo );

% init BLV meshgrid in 3D, define customized RESOLUTION
[BLVX, BLVY, BLVZ] = meshgrid( 1:size(M,2), 1:size(M,1), min(distance(:)):1:max(distance(:))) ; % EDIT HERE FOR DIFFERENT VOLUME RESOLTION

% run interpolation
BLVinterp3 = interp3(1:size(M,2), 1:size(M,1), distance, M, double(BLVX), double(BLVY), double(BLVZ), 'linear');

% compute new (virtual) slice thickness (anatomical thickness corresponding to one BLVinterp3 slice)
sliceThicknessVirtual = (max(distance) - min(distance))/size(BLVinterp3,3); 

end

