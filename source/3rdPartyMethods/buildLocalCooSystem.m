% buildLocalCooSystem.m
% J Zimmermann 2014 

% NOTE: axes in columns !

function [ localCooSystem ] = buildLocalCooSystem( dicinfo )

localCooSystem = [dicinfo(1).ImageOrientationPatient(1:3)';
                    dicinfo(1).ImageOrientationPatient(4:6)';
                    computeThroughPlaneVec(dicinfo)'];
localCooSystem = localCooSystem'; % axes in cols

end

