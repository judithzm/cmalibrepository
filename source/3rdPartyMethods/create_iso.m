% This function creates a Matlab surface struct from a binary stack of
% images based on the given isovalue

function surface = create_iso(image, iso_val)

image_smooth = smooth3(image);
surface = reducepatch(isosurface(image_smooth, iso_val),0.1);

end
