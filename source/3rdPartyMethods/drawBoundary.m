function [ mask ] = drawBoundary( I )

mask = zeros(size(I));

figure;
for sliceIndex = 1:size(I,3)
    imagesc(I(:,:, sliceIndex)); colormap gray;
    mask(:,:,sliceIndex) = createMask(imfreehand);
end
close;

end

