function plotPdfCdf( imvol )
imvol = imvol(:);


[count, value] = hist(imvol, 200);

figure;
plot(value, count, 'b'); %PDF
hold on;

%cumulative density function
cdf = zeros(1,length(value));
for i = 1:length(value)
    cdf(i) = sum(count(1:i));
end

plot(value, cdf, 'r');

end

