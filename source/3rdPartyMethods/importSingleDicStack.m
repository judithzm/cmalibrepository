% importSingleDicStack.m
% imports stack of dicom images into 3D double matrix

% NOTE: all dicoms have to be included in one folder, user input required

% (c) Judith Zimmermann
% 2013

%% IMPORT: single dicom stack 
function [imvol, dicinfo] = importSingleDicStack(directory)

[imvol, dicinfo] = dic2stack(directory);
% check order, flip if not apex2base SA standard
if checkSliceOrderDICOM(dicinfo) ~= 1
    dicinfo = dicinfo(end:-1:1);
    imvol = imvol(:,:,end:-1:1);
end


end