function [ EdgeCanny ] = canny( Image )


% pre: Compute the Magnitude of the gradient
M_x =  [0 0 0; 0.5 0 -0.5; 0 0 0];              %% Mask gradient x direction
M_y =  [0 0.5 0; 0 0 0; 0 -0.5 0];              %% Mask gradient y direction
G_x =  imfilter(Image, M_x, 'symmetric');       %% Gradient in x direction
G_y =  imfilter(Image, M_y, 'symmetric');       %% Gradient in y direction

M   =  sqrt(G_x.^2 + G_y.^2 );     %% Gradient Magnitude


% Display
figure; imagesc(M); axis image; colormap jet; axis off; ...
    title('Magnitude before Non maxima suppresion'); axis xy


% STEP1: Non Maxima suppression step
% Initialize the image with supressed non maxima edges
Mn = M; % CHANGE

% a. Compute the gradient direction
alpha = atan2d(G_y,G_x);
alpha_bd = zeros(size(alpha));
figure; imagesc(alpha); axis image; colormap jet; axis off; ...
    title('Gradient Direction'); axis xy; colorbar

a = [0 45 90 135 180]; %% The 4 basic gradient directions (0=180)

for i = 2:s1-1 %rows
    
    for j = 2:s2-1 %cols
        % Compute the nearest basic direction to the local gradient
        % direction, hold it in "nearest"
        
        % flip all directions to the positive side
        if (alpha(i,j) < 0)
            alpha(i,j) = alpha(i,j) + pi;
        end
        
        % compute the nearest basic direction
        for k = 1:length(a)
            %first iteration: initialize
            if k == 1
            mindiff = abs(radtodeg(alpha(i,j))-a);
            nearestBD = a(k);
            continue;
            end
            %all other iteration: compare
            diff = abs(radtodeg(alpha(i,j))-a);
            if(diff<mindiff)
                nearestBD = a(k);
            end 
        end
        
        % set 180 direction by 0 direction
        if (nearestBD == 180);
            nearestBD = 0;
        end
        
        alpha_bd(i,j) = nearestBD; %fill in alpha_bd (appr by basic directions
        
         
        % Get the two neigbors in opposite directions
        % (4 possibilities)
        if alpha_bd(i,j) == 0
            % east-west
            n1 = sqrt(G_x(i,j+1)^2 + G_y(i,j+1)^2); %gradient magnitude neigbor 1
            n2 = sqrt(G_x(i,j-1)^2 + G_y(i,j-1)^2); %gradient magnitude neigbor 1
        elseif alpha_bd(i,j) == 45
            % north east - south west
            n1 = sqrt(G_x(i-1,j+1)^2 + G_y(i-1,j+1)^2);
            n2 = sqrt(G_x(i+1,j-1)^2 + G_y(i+1,j-1)^2);
        elseif alpha_bd(i,j) == 90
            % north-south
            n1 = sqrt(G_x(i-1,j)^2 + G_y(i-1,j)^2);
            n2 = sqrt(G_x(i+1, j)^2 + G_y(i+1,j)^2);
        elseif alpha_bd(i,j) == 135
            %north west-south east
            n1 = sqrt(G_x(i-1,j-1)^2 + G_y(i-1,j-1)^2);
            n2 = sqrt(G_x(i+1,j+1)^2 + G_y(i+1,j+1)^2);
        end
        
        % And check if the gradient magnitude is smaller than the
        % gradient magnitude of one of its neighbours in the chosen
        % direction. In this case, set it to zero in Mn.

        if sqrt(G_x(i,j)^2 + G_y(i,j)^2) < n1 || sqrt(G_x(i,j)^2 + G_y(i,j)^2) < n2
            Mn(i,j) = 0;
        end
        
    end
end


% Display
figure; imagesc(Mn); axis image; colormap jet; axis off; ...
    title('Magnitude after Non maxima suppresion'); axis xy



% STEP2: Hysteresis
% Create the strong pixel map, high intensities
Th = 0.12;                           %% high treshold 0.5      

StrongPixel = (Mn > Th);        % CHANGE

% Create the weak pixel map, low intensities
Tl = 0.07;                           %% low treshold 0.2

WeakPixel = (Mn > Tl)-StrongPixel;  % CHANGE

% Display
figure; imagesc(StrongPixel); axis image; colormap gray; axis off; ...
    title('Strong Pixel'); axis xy
figure; imagesc(WeakPixel); axis image; colormap gray; axis off; ...
    title('Weak Pixel'); axis xy

% Find the connected component (8-connectivity, use the function bwlabel)
[C, num] = bwlabel(Mn,4); %num: number of connected components 

% Compute the final edge map "EdgeCanny"
EdgeCanny = zeros(size(M)); %% Initialize

% For every connected component, check if one of the pixels is a "strong
% pixel". In this case, set the whole component to be an edge
for k = 1:num
        %get pixel position in CC with k label
        [x,y] = find(C==k);
        %check if any [x,y] pixel is a strong pixel
        if(any(StrongPixel(x,y) == 1))
            %label whole CC as an edge
            EdgeCanny(x,y) = 1;
        end   
end

% Display
figure; imagesc(EdgeCanny); axis image; colormap gray; axis off; axis xy;...
    title('Final Canny output');

end

