% uses best neighbor interpolation

function B = image_translate(A,t)


B = A*0; %return param
d1 = size(B,1);
d2 = size(B,2);

%interpolating input translation vector
t1=round(t(1));
t2=round(t(2));
        
    % backward warping
    for xb=1:d1
        for yb=1:d2 
            %translate
            xa=xb-t1;
            ya=yb-t2;
            
            if ( xa>0 && ya>0 && xa<=d1 && ya<=d2 )
                B(xb,yb) = A( xa , ya );  
            end
             
        end 
    end
        
end %end image_translate(..)

