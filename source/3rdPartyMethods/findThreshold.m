% findThreshold.m
% 
% //brief:
% findThreshold performs the iterative kmeans clustering algorithm
% This is a modified version of kmeans.m, to be used for 3D data stacks
% findThreshold.m is modified for fast performance

% //params:
% input:    imvol  3D image stack
%            m   user-defined mean intensity
% 
% return:   T   thresholds
% 
% //(c) Judith Zimmermann
% //date 10/28/13

function [ T ] = findThreshold( imvol, m )

disp('running findThreshold');

% downsample 2: (in 3 dimensions), 1/(2^3) smaller stack, TODO: think about
% 3D downsampling for non isotropic data !!
imvol_ds = GPReduce(imvol);

% eliminate background/zeros
I = imvol_ds(:);
I(I == 0) = [];
plotHistogram(I);

%% do kmeans clustering on I

% INIT mean intensities, 3 clusters (background, non blood tissue, blood pool)
m0 = ones(size(m)); % helper
K = length(m); 
iter = 1;

% ITER
while any(m-m0)

    m0 = m; % save current means
    
    % 1st step: assign everz pixel to cluster
    for pixelIndex = 1:length(I)
        [~,c(pixelIndex)] = min(abs(I(pixelIndex)-m)); 
    end
    
    % display init means and thresholds for iter == 1
    if iter ==1 %% %% Display initialization
        hist(I,100); title('KMEAN - Histogram - Initialization'); 
        for k = 1:K
            hold on; plot(m(k)*ones(1,1500),1:1500,'g','Linewidth',2); 
        end
        for k = 1:K-1
            hold on; plot((m(k)+m(k+1))/2*ones(1,1500),1:1500,'r','Linewidth',1); 
        end
        hold off
        pause(0.1)
    end
    

    % 2nd step: update clusters
    for k = 1:K  
        m(k)=mean(I(c==k));     % compute the mean intensity in this class
    end

    
    % display updated means and thresholds
    hist(I,100);title(['KMEAN - Histogram - Iter ' num2str(iter)]); 
    for k = 1:K
        hold on; plot(m(k)*ones(1,1500),1:1500,'g','Linewidth',2);
    end
    for k = 1:K-1
        hold on; plot((m(k)+m(k+1))/2*ones(1,1500),1:1500,'r','Linewidth',1);
    end
    hold off;
    drawnow
    pause(0.1)
    
    
    % update flag
    iter = iter +1;

end

% compute final thresholds
T = zeros(1, K-1);
for k = 1:K-1
    T(k) = (m(k)+m(k+1))/2;
end 

disp('computed threshold:')
T

end % end of findThreshold.m

