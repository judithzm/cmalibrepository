%DIC2STACK examines DICOM directory and creates matrix representing a 3D stack
%this is a my wrapper method for dicomread()

% (c) Judith Zimmermann
% date 09/18/13


function [stack, dicinfo] = dic2stack(directory)

dirData = dir(directory);

dirIndex = [dirData.isdir];     
fileList = {dirData(~dirIndex).name}'; %dirData arraz of structs

%sort, necessary?
[fileList]=sort_nat(fileList);

directory = strcat(directory, '/');

% get first file of series and its dicomheader
singleImage = dicomread([directory,fileList{1}]);
%dicinfo = dicominfo([directory,fileList{1}]);

%determine dimensions using first file
numi = size(singleImage,1);
numj = size(singleImage,2);
numSlices = length(fileList);


% fill up stack
stack = zeros(numi,numj,numSlices);

for i=1:numSlices
    stack(:,:,i) = dicomread([directory,fileList{i}]);
    dicinfo(i) = dicominfo([directory,fileList{i}]);     
end

end % end of dic2stack.m


