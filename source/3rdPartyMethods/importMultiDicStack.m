% importMultiDicStack.m
% imports stack of dicom images into 3D double matrix

% NOTE: all dicoms have to be included in one folder, user input required
% NOTE: input data are 4D cine MRI, only considering first frame of each
% series, discarding all other frames

% (c) Judith Zimmermann
% 2013


function [ imvol, dicinfo ] = importMultiDicStack( foldernameInput, subfolder, sliceIndicator )

directory = fullfile(pwd, 'InputDataRAW_multiStack', foldernameInput); 

%iterate subfolders
for i=1:length(sliceIndicator)
    subfoldernameCurrent = strcat(subfolder, num2str(sliceIndicator(i)));
    subdirectoryCurrent = fullfile(directory, subfoldernameCurrent); 
    [imvolCurrent, dicinfoCurrent] = dic2stack(subdirectoryCurrent);
        
    %consider first frame of current volume (to be processed)
    imvol(:,:,i) = imvolCurrent(:,:,1);
    %get corresponding dicom header (first frame)
    dicinfo(i) = dicinfoCurrent(1);
end

imvol = mat2gray(double(imvol));

clear imvolCurrent; clear dicinfoCurrent; clear subdirectoryCurrent; clear subfoldernameCurrent; clear subfolder; clear numImages;

end

