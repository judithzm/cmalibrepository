function [ R ] = defineRotation( wcs, ics )

% define rotation matrix from sourceCooSystem to destCooSystem ICS - all base vectors are unit vectors !
R = [ dot(ics(:,1), wcs(:,1)), dot(ics(:,1), wcs(:,2)), dot(ics(:,1), wcs(:,3));
        dot(ics(:,2), wcs(:,1)), dot(ics(:,2), wcs(:,2)), dot(ics(:,2), wcs(:,3));
        dot(ics(:,3), wcs(:,1)), dot(ics(:,3), wcs(:,2)), dot(ics(:,3), wcs(:,3))
    ];


end

