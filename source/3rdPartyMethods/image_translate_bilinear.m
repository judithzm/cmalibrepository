function B = image_translate_bilinear(A,t)
B = A*0; %return param
d1 = size(B,1);
d2 = size(B,2);

%interpolating input translation vector
t1=round(t(1));
t2=round(t(2));

% backward warping
for xb=1:d1
    for yb=1:d2 

        %translate
        xa=xb-t1;
        ya=yb-t2;


        %BILINEAR INTERPOLATION
        x_integer = floor(xa); 
        y_interger = floor(ya);
        dx = xa - x_integer;
        dy = ya - y_interger;

        if ( x_integer>0 && y_interger>0 && x_integer+1<=d1 && y_interger+1<=d2 )
            B(xb,yb) = A(x_integer,y_interger).*(1-dx).* (1-dy) + A(x_integer+1,y_interger).*dx.*(1-dy)...
                    + A(x_integer, y_interger+1).*(1-dx).*dy+ A(x_integer+1,y_interger+1).*dx.*dy;
        end 
        
    end
end

end

