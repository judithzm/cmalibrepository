% csv2roi.m

% brief:
% reading multiple rois from single csv file (e.g. endo, epi, ...)
% INPUT:  filename name of csv file
%         directory of <filename> to read
% RETURN: roi1, roi2, roi3: seperated rois

% (c) Judith Zimmermann
% date 10/22/2013

function [ roi ] = csv2roi( directory, filename, scanner )

fileDirectory = fullfile(directory, filename); 
rois = csvread(fileDirectory, 1, 15); %start reading at row 1, col 15

numberPoints = size(rois,2)/5; % each point in space consists of 5 entries (3 scanner coo, 2 xy pixel position wrt current plane)
roi = reshape(rois(1,:), 5, numberPoints);

if scanner == 1
%reduce to scanner coordinates, discard xy pixel position
roi(4:5,:) = [];
else
%reduce to pixel coordinates, discard xyz scanner coordinates
roi(1:3,:) = [];
end

end

