% CMAbench.m

% **brief:
% This is the main interface for running the CMAlib
% defining folder directories of raw image data
% calling PT methods for processing different cardiac features
% calling writeXML to save patient specific xml file in specified folder

% **TODO: CMAbench gui to be implemented in here

% please read readme.txt file for more detailed information on the CMAlib!

% (c) J Zimmermann, CMAlib 2014

% *************************************************************************


clc; clear all; close all;

disp('Run CMAbench ...');

% INIT PATH
global CMAlibDir;
CMAlibDir = '/Users/judithzm/Documents/MATLAB/CMAlib'; % EDIT HERE when running on different machine!
cd(CMAlibDir); 
addpath(genpath(CMAlibDir));

global directoryBlackBlood;
global directoryMRA;
global directoryLGE;
global studyName;

%% DEFINE studyName, xml file will be named <studyName>.xml
studyName = input('TYPE STUDY NAME:', 's');

% DEFINE data directories, init with zero if no data available..
disp('Initializing data directories..');
% black blood data
disp('specify black blood data folder');
directoryBlackBlood = uigetdir(CMAlibDir, 'specify directoryBlackBlood')
% LGE data
disp('specify LGE data folder');
directoryLGE = uigetdir(CMAlibDir, 'specify directoryLGE')
% MRA data
disp('specify CEMRA data folder');
directoryMRA = uigetdir(CMAlibDir, 'specify directoryMRA')


%% run PT scripts---- ------------------------------------------------------
patchList = [];

    % Black Blood
    if directoryBlackBlood == 0
        disp('No directory for black blood data defined .. will not perform any shell/fat processing');
    else

        % call cardiacShellPT
        [fvEPI, Mepi] = cardiacShellPT;
        patchList = [patchList, fvEPI];

        % call fatPT
        fvFAT = fatPT(Mepi);
        patchList = [patchList, fvFAT];

    end

    % LGE MRI
    if directoryLGE == 0
        disp('No directory for LGE MRI data defined .. will not perform any scar processing');
    else
        fvSCAR = scarPT;
        patchList = [patchList, fvSCAR];
    end

    % CEMRA
    if directoryMRA == 0
        disp('No directory for CEMRA data defined .. will not perform any vascular processing');
    else
        % do vasc structure processing
        fvVASC = vascPT;
        patchList = [patchList, fvVASC];
    end


%% finalize: write XML file
if isempty(patchList)
    disp('patchList empty.. will not create xml file for this study');
else
% render all surfaces in patchList
renderSurfaceAll(patchList);
% write xml
writeXML(studyName, patchList);
end


disp('End of CMAbench.');
% end of CMAbench.m -----------------------------------------------------
