% ucdf.m

% This is a first draft main script for processing high res ex vivo data
% part of: UCSF gel study

% Judith Zimmermann

% 01/02/14

%% load data
clear all, close all;

foldernameInput = 'phantom_191_tse'; %EDIT HERE

directory = fullfile(pwd, 'InputDataRaw_singleStack/PorcineGelStudyUCSF', foldernameInput); 

% load
[imvol, dicinfo] = dic2stack(directory);

imvol = mat2gray(imvol);

%% region growing on gel pads (high intensity in tse, low intensity in flash)

% smooth regions for possible outliers
imvol_smoothed = smooth(imvol, 'median');

%% extract gel pad
sliceIndicator = 70;

% run region growing
figure; imagesc(imvol_smoothed(:,:,sliceIndicator)); colormap gray; hold on;
[col,row] = ginput(1); 
col = round(col); row = round(row);
[poly,binaryMask] = regionGrowing(imvol_smoothed, [row,col, sliceIndicator], 0.1);

%create patch
fvGEL = create_iso(binaryMask, 0.1); 
fvGEL.surfaceType = 'GEL';

% transform to scanner coordinates
fvGEL.vertices = transform2scannerMod(dicinfo, dicinfo(1).SliceThickness, fvGEL.vertices);

%% show result
figure; 
[surface, image] = iimage_3Dicom_modified(imvol_smoothed(:,:,sliceIndicator), dicinfo(sliceIndicator));
freezeColors;
hold on;
renderSurface(fvGEL,'magenta', 'non');