% FatPT.m
% processing (main) script (main interface) for extracting and rendering fat out of a merged dicom stack 

% NOTE;  edits are only allowed at EDIT HERE!

% (c) Judith Zimmermann
% date 12/04/2013

function [fvFAT] = fatPT(Mepi)

disp('running fatPT...'); drawnow;

global directoryBlackBlood;

%% LOAD: 
% (1) RAW MRI stack
[imvol, dicinfo] = dic2stack(directoryBlackBlood);
% check order, flip if not apex2base SA standard
if checkSliceOrderDICOM(dicinfo) ~= 1
    dicinfo = dicinfo(end:-1:1);
    imvol = imvol(:,:,end:-1:1);
end

% added as parameter, return param in cardiacShellPT()
% % (2) ROI CSV
% [filenameROI filenameROI_pathName]  = uigetfile('*.*', 'fatPT: specify csv ROI file for epi border');
% Mepi = readCSV(filenameROI, dicinfo, filenameROI_pathName);
% % check order, flip if not apex2base SA standard
% if checkSliceOrderCSV(filenameROI, filenameROI_pathName) ~= 1
%     Mepi = Mepi(:,:,end:-1:1);
% end


% (3) PERI mask
% filename = 'richardFAT.csv';
% Mperi = readCSV(filename, dicinfo);
% % check order, flip if not apex2base SA standard
% if checkSliceOrderCSV(filename) ~= 1
%     Mperi = Mperi(:,:,end:-1:1);
% end
% 
% % FAT LAYER by subtraction, assuming fat == pericardial space
% Mfat = Mperi - Mepi;


%% FAT SEGMENTATION, REGION BASED method using a priori info: epicardial shell, thickness constraint
%equalize histogram, increase contrast.
imvolEq = histoEq(imvol, 0.1,0.85);
imvolEq = mat2gray(imvolEq);

%smooth imvol using anisotropic difussion
%imvolEq_smoothed = smooth(imvolEq, 'anisotropic');

%run fat segmentation
Mfat = doFatSegmentation(imvolEq,  Mepi);

%show segmentation results, overlaid
for index = 1:size(imvolEq,3)
    if any(Mfat(:,:,index)) == 0
        continue; % do not plot unsegmented slices
    end
    figure;
    imagesc(imvolEq(:,:,index)); colormap gray; axis equal; freezeColors; hold on;
    contour(Mfat(:,:,index)); colormap jet;
end



%% CREATE surface
%iterpolate first
[Minterp, sliceThicknessVirtual] = interpolateMask3D(Mfat, dicinfo);
Minterp(Minterp >= 0.5) = 1;
Minterp(Minterp < 0.5) = 0;

fvFAT = create_iso(Minterp, 0.5); 
fvFAT.surfaceType = 'FAT';

% transform to scanner coo
figure;
fvFAT.vertices = transform2scannerMod(dicinfo, sliceThicknessVirtual, fvFAT.vertices);

renderSurface(fvFAT, 'yellow', 'non');

disp('finished fatPT ...'); drawnow;


end