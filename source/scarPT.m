% scarPT.m
% processing script (main interface) for extracting and rendering scar out
% of 2D LGE wideband images

% NOTE;  edits are only allowed at EDIT HERE!

% (c) Judith Zimmermann
% date 12/04/2013

function [fvSCAR] = scarPT()

disp('running scarPT ...'); drawnow;

global directoryLGE;

%% IMPORT RAW DATA 

[imvol, dicinfo] = dic2stack(directoryLGE);
% check order, flip if not apex2base SA standard
if checkSliceOrderDICOM(dicinfo) ~= 1
    dicinfo = dicinfo(end:-1:1);
    imvol = imvol(:,:,end:-1:1);
end


%% Scar segmentation
% pre processing
imvolEq = histoEq(imvol, 0.05, 0.95);
imvolEq = mat2gray(imvolEq);

% (option 1) define priors ..
disp('drawBoundary LV EPI');
lvEpiMask = drawBoundary(imvolEq);
disp('drawBoundary LV ENDO');
lvEndoMask = drawBoundary(imvolEq);


% CONSTRUCTION ZONE
% (option 3) get prior from previously black blood segmented myo
% here: use parameter epimaskBlackBlood, dicinfoBlackBlood
% end CONSTRUCTION ZONE


% run scarSegmentation
Mscar  = doScarSegmentation(imvolEq, lvEndoMask, lvEpiMask);
figure;
imagesc(imvolEq(:,:,3)); colormap gray; freezeColors; axis equal
hold on; contour(Mscar(:,:,3)); colormap jet;


%% create surface mesh in scanner coordinates
% interpolate (dont really want to do that, but right now the only way to
% take gaps into account) MUST !!
[Minterp, sliceThicknessVirtual] = interpolateMask3D(Mscar, dicinfo);

% Creating surface mesh
fvSCAR = create_iso(Minterp, 0.1); 
fvSCAR.surfaceType = 'SCAR';

% transform vertex points
fvSCAR.vertices = transform2scannerMod(dicinfo, sliceThicknessVirtual, fvSCAR.vertices);


%% Render surface, params (1) faces,vertices struct; (2) color
renderSurface(fvSCAR, 'magenta', 'non');

disp('finished scarPT ...'); drawnow;
end