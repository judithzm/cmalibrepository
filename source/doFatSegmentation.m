% doFatSegmentation.m
% 
% ** brief:
% segments epi fat based on a priori epi shell information
% using region growing approach with fat thickness constraint
% careful: this method is very application specific and was developed in a
% way to best make use of black blood haste images. 
% 
% params:   imvolEq_smooted: input 2D stack/or single image (pre smoothing and contrast enhancement recommended)
%           Mepi: (same zie as first parameter) stack of epicardial masks
%
% return:   Mfat: stack of fat masks (same size as Mepi)
% 
% (c) J Zimmermann 2014
% CMAlib


function [ Mfat ] = doFatSegmentation( imvolEq_smoothed, Mepi )

% return
Mfat = zeros(size(imvolEq_smoothed));

% pre allocate
epimaskNewAll = zeros(size(imvolEq_smoothed));
searchMaskAll = zeros(size(imvolEq_smoothed));

%% (1) for each slice for which an epicardial boundary is defined: define epimask and searchMask
for sliceIndex = 1:size(imvolEq_smoothed,3)

    % init current image and mask
    epimaskNew = Mepi(:,:,sliceIndex); 
    if any(epimaskNew(:)) == 0
        continue;
    end
     
    epimaskNewAll(:,:,sliceIndex) = epimaskNew;

    % SEARCHMASK
    % dilate for thickness constraint, define searchMask
    epimaskDilate = imdilate(epimaskNew,ones(10,10)); % 2nd params == structering element, **TODO: to be defined based on expected maximum fat thickness
    searchMask = epimaskDilate-epimaskNew;   
    
    searchMaskAll(:,:,sliceIndex) = searchMask;     
    
end

%% (2) CALCULATE LOCAL FAT THRESHOLG using entire stack for bigger sample size
    % define searchImageAll == dilated epi boundary
    searchImageAll = imvolEq_smoothed.*searchMaskAll;
    helper = searchImageAll(:);
    helper(helper == 0 ) = [];

    % for manual threshold
    figure; hist(helper,200); title('intensity distribution searchImage')   
    fatIntensity = input('type in threshold separating fat intensity pixels: ');
    close;
    % end for manual threshold 
    
%     % for kmeans clustering
%     thresholds  = findThreshold( helper, [0.3 1] ); % returning two thresholds, clustering in 3 clusters.
%     fatIntensity = max(thresholds);
%     % end for kmenas clustering       
    
%% (3) automated seed selection and region grow (everz slice treated seperately)

for sliceIndex = 1:size(imvolEq_smoothed,3)
    
    %definitions for current iteration
    epimask = epimaskNewAll(:,:,sliceIndex);
    %check if epishell is defined for current slice
    if any(epimask(:)) == 0
        continue;
    end
    image = imvolEq_smoothed(:,:,sliceIndex);
    searchMask = searchMaskAll(:,:,sliceIndex);
    searchImage = searchImageAll(:,:,sliceIndex);

    
    %% find seed points 
    seeds = [];
    
    % get perimeter pixel image coordinats of epi shell[counts, values] = hist(imvolNormal(:), 500);
    [epiperimRow, epiperimCol] = find(image.*(bwperim(epimask)));
    
    
    for i = 1:length(epiperimRow)
        r = epiperimRow(i);
        c = epiperimCol(i);

        % define current 4-neighborhood  coordinates
%         nCoo = [r-1,c-1; r-1,c; r-1,c+1;     
%                 r,c-1; r,c; r,c+1;
%                 r+1,c-1;r+1,c; r+1,c+1;];
%             
        % 8-neighborhood
        nCoo = [r-2,c-2; r-2,c-1; r-2,c; r-2,c+1; r-2,c+2;
                r-1,c-2; r-1,c-1; r-1,c; r-1,c+1; r-1,c+2; 
                r,c-2; r,c-1; r,c; r,c+1; r,c+2; 
                r+1,c-2; r+1,c-1; r+1,c; r+1,c+1; r+1,c+2; 
                r+2,c-2; r+2,c-1; r+2,c; r+2,c+1; r+2,c+2];

        % check for highintensity seed pixels within neighboorhood           
        for j = 1:size(nCoo,1)
            if searchImage(nCoo(j,1),nCoo(j,2)) >= fatIntensity
                seeds = [seeds; nCoo(j,:)];
            end
        end

    end

    
%     % show image slice, seed points and searchMask
%     figure; title('image equalized, seed points, search mask'); imagesc(image);
%     colormap gray
%     axis equal
%     if size(seeds,1) > 0 
%     hold on
%     scatter(seeds(:,2), seeds(:,1))
%     contour(searchMask, 'r')
%     else
%         input('No seeds found!')
%         return;
%     end
%     
    
    
    %% (4) DO REGION GROWING
    T = 0.1; 
    seeds = seeds(1:2:size(seeds,1), :);
    RegionAll = zeros(size(image));
    searchImage = image.*searchMask;
    
    for i = 1:size(seeds,1)
        Region = regionGrowing2D( searchImage, seeds(i,:), T, 0 ); % last param: updateMean flag
        RegionAll = RegionAll + Region;
    end
    % mask for this slice
    RegionAll(RegionAll>0) = 1;


    %% finalize fatMask in 2D (== mask of current image slice)
    fatMask(:,:,sliceIndex) = RegionAll;
    
end


%% FP analysis on fatMask
% perform connected component analysis
[fatMask_cca, numLabels] = bwlabeln(fatMask);

componentPixelCount = zeros(numLabels,1);
for i = 1:numLabels
    componentPixelCount(i) = length(find(fatMask_cca(:) == i));    
end

% exclude all components with pixel count <30
smallComponents = find(componentPixelCount < 30);
for i = 1:length(smallComponents)
    fatMask_cca(fatMask_cca == smallComponents(i)) = 0;
    %update componentPixelCount vector
    componentPixelCount(smallComponents(i)) = 0;
end

% TODO... further FP FN analysis ...
%remainingComponents = find(componentPixelCount ~= 0); % needed for further FP FN analysis


%% finalize Mfat in 3D
Mfat(fatMask_cca ~= 0) = 1;

%% filling holes in 3D
mask = [1 1 1 ; 1 1 1; 1 1 1]; %EDIT HERE!
mask3D = cat(3,mask,mask,mask);
Mfat = imclose(Mfat,mask3D);


end % end of doFatSegmentation.m




