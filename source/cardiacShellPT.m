% cardiacShellPT.m

% This is the main script interface for processing MRI images (full cardiac coverage stack) with the aim
% of extracting and displaying cardiac shells (epicardial/endocardiacl
% boundaries).

% NOTE;  edits are only allowed at EDIT HERE!

% (c) Judith Zimmermann
% date 10/22/2013


function [fvSHELL, Mepi] = cardiacShellPT()

disp('running cardiacShellPT ...'); drawnow;

global directoryBlackBlood;

 %% IMPORT
[imvol, dicinfo] = dic2stack(directoryBlackBlood);
imvol = mat2gray(imvol);
% check order, flip if not apex2base SA standard
if checkSliceOrderDICOM(dicinfo) ~= 1
    dicinfo = dicinfo(end:-1:1);
    imvol = imvol(:,:,end:-1:1);
end

% CSV files including epicardial shell
[filenameROI filenameROI_pathName]  = uigetfile('*.*', 'specify csv ROI file for epi shell');
Mepi = readCSV(filenameROI, dicinfo, filenameROI_pathName);

% check order, flip if not apex2base SA standard
if checkSliceOrderCSV(filenameROI, filenameROI_pathName) ~= 1
    Mepi = Mepi(:,:,end:-1:1);
end


%% Create surface
% Interpolate M
[Minterp, sliceThicknessVirtual] = interpolateMask3D(Mepi, dicinfo);
Minterp(Minterp >= 0.5 ) = 1;
Minterp(Minterp < 0.5) = 0;

%close apex and open base of shell
Minterp_reshape = reshape(Minterp, size(Minterp,1)*size(Minterp,2), []);
filledSlices = find(any(Minterp_reshape));
Minterp(:,:,max(filledSlices)+1) = Minterp(:,:,max(filledSlices)); % copy most basal slice
%get perimeter to show hollow cardiac shell
Minterp_perim = bwperim(Minterp,18);
Minterp_perim(:,:,max(filledSlices) + 1) = zeros(size(Minterp(:,:,1)));

%create patch
fvSHELL = create_iso(Minterp_perim, 0.1);
fvSHELL.surfaceType = 'EPI'; % name surface mandatory!

%transform to scanner coo system
fvSHELL.vertices = transform2scannerMod(dicinfo, sliceThicknessVirtual, fvSHELL.vertices);


%% Render surface, params (1) faces,vertices struct; (2) color
renderSurface(fvSHELL, 'blue', 'non');

disp('finished cardiacShellPT ...'); drawnow;

end
