% doScarSegmentation.m
% 
% ** brief:
% finds threshold based on intensity distribution of LV, thresholding, FP
% removal based on scar anatomy assumtions, **TODO: FN adding...
% see Quian Tao paper, MRM 2010, as reference.
% 
% params:   I stack of 2D images
%           lvEndoMask binary mask of LV endocardium 
%           lvEpiMask binary mask of left ventricle (myo plus blood pool)
%
% return:   Mscar: binary stack of scar masks (same size as I)
% 
% (c) J Zimmermann 2014
% CMAlib



function [ Mscar ] = doScarSegmentation( I, lvEndoMask, lvEpiMask )

% set up
% myo ring subtract to obtain LV myocardium
lvMyoMask = lvEpiMask - lvEndoMask;

% mask corresponding images
lvMyoImage = I.*lvMyoMask; 
lvEpiImage = I.*lvEpiMask; 

% define dim
dimI = size(I,1);
dimJ = size(I,2);
dimK = size(I,3);


%% SEGMENTATION 
% find scar threshold by kmeans of pixels contained within lvEpi
Tscar = findThreshold(lvEpiImage, [0.1 0.8]);
close;

% tresholding lvMyoImage using Tscar --> get scarMask
[ scarMask ] = doThresholding( lvMyoImage, Tscar );
scarMask(scarMask ~= max(scarMask(:))) = 0; % choosing region with highest intensity
scarMask(scarMask==max(scarMask(:))) = 1;


%% FP / FN ANALYSIS 
% perform connected component analysis
[scarMask_cca, numLabels] = bwlabeln(scarMask);

componentPixelCount = zeros(numLabels,1);
for i = 1:numLabels
    componentPixelCount(i) = length(find(scarMask_cca(:) == i));    
end

%% false positive analysis
% (a) exclude all components with pixel count <10
smallComponents = find(componentPixelCount < 50);
for i = 1:length(smallComponents)
    scarMask_cca(scarMask_cca == smallComponents(i)) = 0;
    %update componentPixelCount vector
    componentPixelCount(smallComponents(i)) = 0;
end
remainingComponents = find(componentPixelCount ~= 0);

% (b) exclude all components which sit in one single slice (due to tracing error in that slice)
for i = 1:length(remainingComponents)
    
    % get indices of current component
    helperLinearIndices = find(scarMask_cca == remainingComponents(i));
    [~,~,k] = ind2sub([dimI dimJ dimK], helperLinearIndices);
    
    % check k index
    if (max(k)-min(k)) == 0
        % component sits in single slice --> eliminate
        scarMask_cca(scarMask_cca == remainingComponents(i)) = 0;
        % update componentPixelCount
        componentPixelCount(remainingComponents(i)) = 0;
        
    end
end
% update remainingComponents
remainingComponents = find(componentPixelCount ~= 0);

% (c) exclude components representing thin layers connected to epi,endo
% boundary
% this is part of the Quien Tao paper, having doubts whether the anatomical
% assumptions are accurate. (from a scar anatomy perspective.. see transmural scar)

%% TODO: false negative analysis



%% finalize
Mscar = scarMask_cca;
% Mscar = zeros(size(scarMask_cca));
% Mscar(scarMask_cca ~= 0) = 1;

end % end of doScarSegmentation.m

