
% method: RegionGrowing.m, to be called in Exercise2.m (PART A)
% (c) Judith Zimmermann
% date: 07/13/13

function Region = RegionGrowingJZ(Image,T)

% params: 
% I: gray scale image to be processed
% T: defined threshold for deciding whether or not visited pixels belongs to
% region

[s1, s2] = size(Image); 
Visited = zeros(s1,s2);
Region = zeros(s1,s2);
neighbor = [-1 0; 1 0; 0 -1;0 1; 1 1; -1 -1; -1 1; 1 -1];
t = tic;

% ****INIT: Select and add the seed point to the the queue 
figure; imagesc(Image);axis image; axis xy; title('Select the seed point');...
    colormap gray;axis off;
[y,x] = ginput(1); %x==row, y==col
x = round(x); % row index
y = round(y); % col index

Imean = Image(x,y); % initial mean intensity, to be updated.
%list = zeros(10000,2);
list(1,:) = [x, y]; %add seed pixel to the list

%lengthList = nnz(list(:,1));

% ****ITER: Grow until FIFO list only contains zero pixels****************

%while nnz(list(:,1)) %while queue is not empty
while (size(list,1) ~= 0)       %CHANGE
    
    % Pick up first pixel from the pixel indices list
    %pixel_curr = Image(list(1,1), list(1,2));
    
    pixel_curr_cooX = list(1,1);
    pixel_curr_cooY = list(1,2);
    
    list(1,:) = []; %delete first list entry
    %lengthList = lengthList - 1;

    % Check if pixel is homogeneous, use: abs(pixel_curr - Im) <= T.
    %if(abs(pixel_curr - Imean) <= T)    
    if(abs(Image(pixel_curr_cooX, pixel_curr_cooY) - Imean) <= T) %CHANGE
        
        % label it as homogeneous, i.e. it belongs to Region
        Region(pixel_curr_cooX, pixel_curr_cooY) = 1;
        
        % add neighbors (i.e. their pixel indices) to the list
        for k = 1:8
            % check if neighbor has been visited before: if false --> add
            if( Visited( pixel_curr_cooX + neighbor(k,1) , pixel_curr_cooY + neighbor(k,2)  ) == 0 )
       
                Visited(pixel_curr_cooX + neighbor(k,1) , pixel_curr_cooY + neighbor(k,2)) = 1; 
                list = [list; pixel_curr_cooX + neighbor(k,1) , pixel_curr_cooY + neighbor(k,2)] ; %current pixel plus neighbor offset

            end
        end
        
        
        % update mean intensity Im of Region
        numPixelsInRegion = nnz(Region); %including new pixel
        Imean = ( (numPixelsInRegion-1) * Imean + Image(pixel_curr_cooX , pixel_curr_cooY ) ) / numPixelsInRegion;
  
    end %END homogeneity check
        
    % in case pixel is inhomogeneous: only update 'Visited' matrix
    Visited(pixel_curr_cooX, pixel_curr_cooY) = 1;    
    
    if toc(t)>1/23
        imagesc(Image+Region,[0 1]); axis image; colormap gray; axis off; axis xy;
        drawnow;
        t = tic;
    end
    
end % end of WHILE

%% Display
imagesc(Image+Region,[0 1]); axis image; colormap gray; axis off; axis xy;
drawnow;

