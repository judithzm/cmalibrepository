function [ prob, mask] = solveRw_test( A,seeds,labels, beta)

%% **brief: defines random walk algorithm
% (c) Judith Zimmermann 06/27/2013

% linear system of equation is set up in steps (1) to (6) 
% step (7): solving L X = Bt M



% (1) compute weights matrix W
Avec = A(:); %treat image matrix as vector
W = sparse(length(Avec), length(Avec)); %weight matrix, --> sparse later on

for index = 1:length(Avec) %350*464 iterations
    
    %**get coo of center pixel in A 
    [row, col] = ind2sub(size(A), index);
    
    %**filling in diff intensity values, using 4-connectivity
    %** NOTE: if conditions make sure boundaries are treated correctly
    if(col>1)
    left = A(row, col-1); %left neighbor intensity
    W(index, sub2ind(size(A),row, col-1)) = abs(A(row, col) - left);
    end
    
    if(col<size(A,2))
    right = A(row, col+1); %right neighbor intensity
    W(index, sub2ind(size(A),row, col+1)) = abs(A(row, col) - right);
    end
    
    if(row>1)
    top = A(row-1, col); %top neighbor intensity
    W(index, sub2ind(size(A),row-1, col)) = abs(A(row, col) - top);
    end
    
    if(row<size(A,1))
    bottom = A(row+1, col); %bottom neighbor intensity
    W(index, sub2ind(size(A),row+1, col)) = abs(A(row, col) - bottom);
    end
    
end


% (2) normalize W
W = (W - min(W(:)))./(max(W(:)) - min(W(:)) + eps);

%
% (3) compute exponential weights
nzindex = find(W); % linear indices of nonzero entries in W
e = 10.^(-6);
W(nzindex) = ( exp(-beta*W(nzindex)) + e );

%
% (4) compute Laplacian L = D - W
% DEGREE MATRIX: sum of row entries in W on diagonal
D = sum(W(:,:)) ; 
D = diag(D);

% LAPLACIAN L
%L = sparse( size(W,1), size(W,2) );
L = D - W;

% (5) Assemble sparse LU and B 
% pre-step: eliminate all seed rows in L --> L_mod 

LU = L;
LU(seeds,:) = [];

% eliminate all seed cols in L_mod --> LU
% in addition: eliminated cols --> Bt

Bt = LU(:,seeds);
LU(:,seeds) = [];

% %(6a) Assemble M ---> solving for 2 labels !
% M = sparse(length(seeds), 2);
% M(:,:) = 0;
% for i = 1:length(seeds)
%    M(i, labels(i)) = 1; %label '1': forground; label '2': background
% end

%(6b) Assemble M ---> solving for 1 label (foreground label)

M = sparse(length(seeds), 1);
M(:,:) = 0;

M(labels==1) = 1;

% (7) Solve System LU * X = Bt * M using backslash
rightSide = sparse(-Bt*M); 
X = LU\rightSide;

% (8) create probability MATRIX (same size as original image) 
% a. first: label all marked nodes (seeds) with 1 (foreground seeds) and 0 (background seeds) respectively
prob = zeros( size(A,1), size(A,2) ); 
num_f_seeds = length(find(labels == 1));
f_seeds = seeds(1:num_f_seeds); %linear coo of definite foreground pixels, i.e. foreground seeds
prob(f_seeds) = 1; %all others (marked background + unmarked) == 0

% b. second: insert probability values (X) for unmarked nodes, skip marked nodes (=seeds)
% following should hold: length(X) = length(A_vec) - length(seeds);
index = 1;
for i = 1:length(Avec)
    if(any(seeds == i)) %when hitting an already marked node: true
        continue;
    else
        %assign prob value to prob(i) ...
        prob(i) = X(index);
        index = index +1;
    end
end


% (9) create mask MATRIX (same size as original image)
mask = 2*ones(size(A,1), size(A,2)); %init: all pixeled marked as background pixels
mask( prob > 0.5) = 1;


end %end of solveRw.m

