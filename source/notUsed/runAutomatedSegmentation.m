
% runAutomatedSegmentation.m
% This script includes a pre processing methods, random walks segmentation, region growing.
% Please uncomment where necessary;

% NOTE;  edits are only allowed at EDIT HERE!

% (c) Judith Zimmermann
% 2013

% NOTE: variables imvol and sliceIndicator should be in current workspace !
    
    %% LINEAR SMOOTHING (isotropic diffusion)
    % mask = fspecial('gauss', 7, 1.5);
    % imvol_f = imvol;
    % for i=1:size(imvol,3)
    %     imvol_f(:,:,i) = imfilter(imvol(:,:,i), mask, 'symmetric');
    % end


    %% LINEAR /NON LINEAR SMOOTHING 
    for i = 1:size(imvol,3)

        %Define sigma for edge indicator function (anisotropic diffusion)
        sigma = 0.005; %EDIT HERE! -- around 0.01 for cardiac cines

        %do smoothing, use: function [ I_heateq, I_edge, I_median, sigma ] = doNonLinearSmoothing( Image )
        [I_iso_current, I_aniso_current, I_median_current] = doNonLinearSmoothing(imvol(:,:, i), sigma);
        
        I_iso(:,:,i) = I_iso_current;
        I_aniso(:,:,i) = I_aniso_current;
        I_median(:,:,i) = I_median_current;
    end
    
    
    %% GRADIENT IMAGES
    I = I_aniso(:,:,15);
    
    Mx = [0 0 0; 0.5 0 -0.5; 0 0 0]; % grad in x direction
    My = [0 0.5 0; 0 0 0; 0 -0.5 0]; % grad in y direction
    
    I_gradx = conv2(I,Mx);
    I_grady = conv2(I,Mx);
    
    I_mag = sqrt(I_gradx.^2 + I_grady.^2);

    %% EDGE BOOSTING

    image = imvol(:,:,17);      %EDIT HERE
    
    sigma = 3;                  %EDIT HERE
    mask = fspecial('gauss', 12, sigma);
    image_sigma = imfilter(image, mask, 'symmetric'); %smoothed
    detail = image-image_sigma; %lost detail
    
    I_boosted = image + detail;
    
    figure; imagesc(image); colormap gray; axis equal;
    figure; imagesc(detail); colormap gray; axis equal;
    figure; imagesc(I_boosted); colormap gray; axis equal;


    
    
    %% RANDOM WALKS SEGMENTATION
    
    I_aniso = imvol;
    
    for i = 1:size(imvol,3)
        maskRW = runRandomWalks_test(I_aniso(:,:,i));

        mask = [0 0 1 0 0; 0 1 1 1 0; 1 1 1 1 1; 0 1 1 1 0; 0 0 1 0 0]; %EDIT HERE!
        maskRWmorp_current = imclose(imopen(maskRW,mask), mask);
        
        maskRWmorp(:,:,i) = maskRWmorp_current;

        figure; imagesc(imvol(:,:, i)); colormap gray; freezeColors; 
        hold on;
        contour(maskRWmorp_current); colormap cool; axis equal;
        
    end
    
    %% MANUAL SEGMENTATION USING imfreehand()
    figure; 
    for i = 1:size(imvol,3)
        imagesc(imvol(:,:,i)); axis equal; colormap gray; 
        h = imfreehand;
        maskMAN(:,:,i) = createMask(h);
    end
    
    %% run region growing
    singleSlice = I_boosted;
    maskRG = RegionGrowingJZ_test(singleSlice, 0.005);
    
    mask = [0 1 1 1 0; 0 1 1 1 0; 0 1 1 1 0]; %EDIT HERE!
    maskRGmorp = imclose(imopen(maskRG,mask), mask);
    
    figure; imagesc(singleSlice); colormap gray; freezeColors; 
    hold on;
    contour(maskRGmorp); colormap cool; axis equal;

    %% finalize
    M = mask;
    
    input('press ENTER for closing figures and continue script'); close all; clear mask, clear maskRW; clear maskRWmorp; clear maskRWmorp_current;

