%   Random Walks for Image Segmentation

%   ORIGINAL IMPLEMENTATION:
%   Author: Athanasios Karamalis
%   Date: 08.05.2012
%   CAMP II - Advanced Image Segmentation
%
%   Based on the paper:
%   Leo Grady, "Random Walks for Image Segmentation", IEEE Trans. on Pattern 
%   Analysis and Machine Intelligence, Vol. 28, No. 11, pp. 1768-1783, 
%   Nov., 2006.
%   
%   Available at: http://www.cns.bu.edu/~lgrady/grady2006random.pdf
%   Original code available at http://cns.bu.edu/~lgrady/software.html

%   INTERFACE HAS BEEN MODIFIED !!! 
%   J Zimmermann 
%   DATE: 10/23/13

function [mask] = runRandomWalks_test(A)

    h = figure; imagesc(A); colormap gray; axis equal; axis xy;
    % Place foreground seeds manually
    title('Place foreground seeds manually')
    [ seedF, labelF ] = drawSeeds(h,size(A),1);
    % Place background seeds manually
    title('Place background seeds manually')
    [ seedB, labelB ] = drawSeeds(h,size(A),2);
    % Close image for now
    close(h);

    % Create COLUMN vectors of seeds and labels for Random Walks
    seeds = [seedF; seedB];
    labels = [labelF; labelB];

% Show seeds
showSeeds(A,seeds,labels)

% Solve random walks problem --> implement this method
tic
[ probabilities, mask] = solveRw_test( A,seeds,labels,90);
toc

% define background as == 0
mask(mask == 2) = 0;

end

