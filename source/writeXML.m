% writeXML.m

% brief:
% load multiple surface meshes
% write ONE XML file including all loaded surfaces 
% saves xml file in ~/OutputXML folder

% params: studyName last name of patient, first name if volunteer
%         surfaceType PERI, EPI, LVENDO, RVENDO, LVEPI, SCAR, VASC
%         fv face-vertices struct
%         

% (c) Judith Zimmermann 2013

function writeXML(studyName, fv_array)

global CMAlibDir;

disp('writing patchList into .xml file ..');

surfaces = cell(1,length(fv_array));
names = cell(1,length(fv_array));

for i = 1:length(fv_array)
    surfaces{i} = fv_array(i);
    names{i} = fv_array(i).surfaceType; 
end

fname = strcat(CMAlibDir,'/OutputXML/', studyName, '.xml');
writeEnsiteFile(fname, surfaces, names);

helperName = strcat(studyName, '.xml');
disp(strcat('finished writing: ', helperName));


end % end of writeXML.m